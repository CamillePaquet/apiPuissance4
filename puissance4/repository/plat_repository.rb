require_relative 'i_plat_repository'

module Jeux 
    module OrganisationCarte
        module Repository


            class PlatRepository < IPlatRepository

                def plat_existe_par_nom? nom
                    begin
                        plat = Jeux::OrganisationCarte::Models::Plat.find_by(nom: nom)
                        return true
                    rescue Mongoid::Errors::DocumentNotFound 
                        return false
                    end
                end

                def ajout_plat_repertoire options = {}
                    begin
                        Jeux::OrganisationCarte::Models::Plat.create(
                            nom: options[:nom],
                            description: options[:description],
                            type: options[:type],
                            prix: options[:prix],
                            quantite: options[:quantite]
                        )
                        return "CREATED"
                    end
                end

                def modification_quantite_repertoire options = {}
                    begin
                        plat = Jeux::OrganisationCarte::Models::Plat.find_by(nom: options[:nom])
                        plat.update_attributes!(
                            quantite: options[:quantite]
                        )
                        plat.save!
                        return "UPDATED"
                    rescue Mongoid::Errors::DocumentNotFound 
                        return "NOTFOUND"
                    end
                end

                def recuperation_liste_plats_repertoire
                    begin
                        return Jeux::OrganisationCarte::Models::Plat.all
                        
                    end
                end
            end
        end
    end
end