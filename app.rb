require 'rubygems'
require 'sinatra'
require 'mongoid'
require 'dotenv'
require 'sinatra/namespace'
require_relative 'helpers/init'
require_relative 'puissance4/repository/init'
require_relative 'puissance4/models/init'
require_relative 'puissance4/services/init'

module Jeux
  class Jeux::Backend < Sinatra::Base


    set :port, 9292
    set :bind, '0.0.0.0'

    register Sinatra::Namespace
    Dotenv.load
    Mongoid.load!(File.join(File.dirname(__FILE__), 'config', 'mongoid.yml'))

  end
end

require_relative 'puissance4/controller/init'
